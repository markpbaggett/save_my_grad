# Save My Grad by South-Doyle CodeTn Team

---

## About

*Save My Grad* is a simple time management app designed to give you a decent understanding of all upcoming events developed by students from South-Doyle High School as part of [CodeTN](https://github.com/CodeTN).

The project was built using Python3, Flask, Jinja2 Templates, Bootstrap, and SQLAlchemy.

## Installation and Requirements

If you are installing the application for the first time, you can run the following from the command line:

`./install.sh`

Make sure that you have proper permissions to execute the script before running it:

`chmod 755 install.sh`

## Starting the Server

After the project is installed, you can easily start the server by executing start.sh from the command line:

`./start.sh`

Make sure that you have proper permissions to execute the script before running it

```chmod 755 start.sh```

## Repository Structure

The following diagram describes the layout of the *Save My Grad* application:

```
|-- app
	|-- static
    	|-- css
        	|-- about.css
            |-- account creation.css
            |-- assignments.css
            |-- baseassets.css
            |-- faq.css
            |-- login.css
        |-- images
            |-- completed.png
            |-- create.png
            |-- tag.png
    |-- templates
    	|-- about.html
        |-- accountcreation.html
        |-- assignments.html
        |-- base.html
        |-- edit_assignment.html
        |-- EditProfile.html
        |-- faq.html
        |-- features.html
        |-- index.html
        |-- login.html
    |-- forms.py
    |-- __init__.py
    |-- models.py
    |-- views.py
|-- tests
	|-- dakota_test.py
    |-- FILENAME.py
|-- config.py
|-- db_create.py
|-- dbHandler.py
|-- db_migrate.py
|-- install.sh
|-- README.md
|-- run.py
|-- start.sh

```

** Definitions **:

* /README.md -- this file describes our app and how to get it running.
* /install.sh -- this bash script installs our app and its dependencies.
* /start.sh -- this bash script enables the virtual environment and starts the web app.
* /run.py -- this file starts the web server.  Note:  the virtual environment must be running beforehand.
* /config.py -- this file acts as our main configuration file and includes settings used throughout the application.
* /db_create.py -- this file creates our SQLalchemy database based on options in **/config.py** and **/app/models.py**.
* /db_migrate.py -- this file modifies our database in case we need to change the structure defined in **/app/models.py**.
* /dbHandler.py -- this file includes all our functions for interacting with our database from our controller, **/app/views.py**.
* /app/__init__.py -- this file initializes our application.
* /app/forms.py -- this file defines all of the HTML forms used in our application.
* /app/models.py -- this file defines the layour and structure of our database.
* /app/views.py -- this files acts as the controller of our application.  It handles routing, sessioning, and redirects our renders templates based on actions by the user.
* /app/static/css -- this directory stores all our css.
* /app/static/templates -- this directory contains all our JINJA 2 / HTML templates.
* /app/static/images -- this directory includes static images used by the site.

## Thanks for Examples and Inspiration:

* [Miguel Grinberg's Flask Mega Tutorial](https://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-i-hello-world)
* [W3 Schools Bootstrap Tutorial](http://www.w3schools.com/bootstrap/)
* [Bootstrap Editor and Builder](http://www.bootply.com/)
* [Dozens of StackOverflow Threads like this](http://stackoverflow.com/questions/466345/converting-string-into-datetime)
* [The Official Flask Documentation](http://flask.pocoo.org/docs/0.11/quickstart/#sessions)
* [SQL Designer](http://ondras.zarovi.cz/sql/demo/)
* [Bootstrap Datepicker Docs](https://bootstrap-datepicker.readthedocs.io/en/latest/)