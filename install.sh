nstalling Python3 Virtual Enviornment in hidden directory
echo 'Installing Virtual Environment'
virtualenv -p python3 .projectcalendar || exit

# Enabling Virtual Environment
echo 'Enabling Virtual Environment'
source .projectcalendar/bin/activate

# Installing Dependencies
echo 'Install Flask Stuff'
pip install flask
pip install flask-sqlalchemy
pip install flask-whooshalchemy
pip install flask-wtf
pip install wtforms


echo 'Install SQL Alchemy Stuff'
pip install sqlalchemy
pip install sqlalchemy-migrate

echo 'Install other Python Silliness'
pip install datetime
pip install jinja2

# Setup Database
echo 'Creating db'
python db_create.py

echo 'Done'
