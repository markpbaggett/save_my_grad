from app import app
from flask import render_template, request, redirect, session, url_for
from .forms import LoginForm, AssignmentForm, UserForm, EditForm, StatusForm, EditAssignment
from dbHandler import find_user, find_username, create_user, pass_user_info, pull_user_form_info, update_account_info, get_assignments, add_assignment, does_assignment_exist, change_status, delete_assignment, grab_user_tags, update_assignment

logged_in = "False"

@app.route('/')
@app.route('/index')
def index():
    userform = LoginForm()
    logged_in = "False"
    if 'username' in session:
        logged_in = "True"
    # if request.method == "POST":
    #     # testuser = userform.userlogin.data
    #     # testpassword = userform.passlogin.data
    #     # logged_in = find_user(testuser, testpassword)
    #     # print(str(logged_in))
    #     return render_template('index.html', title='Test', userform = userform, testuser = testuser, testpassword = testpassword, logged_in = logged_in)
    return render_template('index.html', title='Home', userform = userform, logged_in = logged_in)
    
@app.route('/faq', methods = ['GET','POST'])
def faq():
    userform = LoginForm()
    if request.method == "POST":
        testuser = userform.userlogin.data
        testpassword = userform.passlogin.data
        return render_template('index.html', title='Test', userform = userform, testuser = testuser, testpassword = testpassword)
    return render_template('faq.html', title= 'FAQ', userform = userform)
    
@app.route('/about')
def about():
    logged_in = "False"
    if 'username' in session:
        logged_in = "True"
    userform = LoginForm()
    # if request.method == "POST":
    #     testuser = userform.userlogin.data
    #     testpassword = userform.passlogin.data
    #     return render_template('index.html', title='Test', userform = userform, testuser = testuser, testpassword = testpassword)
    return render_template('about.html', title= 'About', userform = userform, logged_in = logged_in)
    
@app.route('/features')
def features():
    logged_in = "False"
    if 'username' in session:
        logged_in = "True"
    userform = LoginForm()
    # if request.method == "POST":
    #     testuser = userform.userlogin.data
    #     testpassword = userform.passlogin.data
    #     return render_template('features.html', title='Test', userform = userform, testuser = testuser, testpassword = testpassword)
    return render_template('features.html', title= 'Features', userform = userform, logged_in = logged_in)
    
@app.route('/assignments', methods = ['GET','POST'])
def assignments():
    if 'username' in session:
        return redirect('/assignments&u={0}'.format(session["username"]))
    else:
        return redirect('/login')

@app.route('/accountcreation', methods = ['GET','POST'])
def accountcreation():
    logged_in = "False"
    createform = UserForm()
    userform = LoginForm()
    if request.method == "POST":
        username = createform.username.data
        fullname = createform.fullname.data
        twitter = createform.emailentry.data
        image = createform.image.data
        password = createform.password.data
        confirmpassword = createform.confirmpassword.data
        if password != confirmpassword:
            message = "Password does not match"
            return render_template('accountcreation.html', title='AccountCreation', userform = userform, createform = createform, message = message)
        # find_username = createform.find_username.data
        does_user_exist = find_username(username)
        if does_user_exist == "False":
            createuser = create_user(username, fullname, password, twitter, image)
            message = "Welcome, {0}. You are now signed in.".format(createuser)
            userinfo = pass_user_info(username)
            return render_template('accountcreation.html', title='AccountCreation', userform = userform, createform = createform, message = message)
       
        else:
            message = "This account already exists."
            return render_template('accountcreation.html', title='AccountCreation', userform = userform, createform = createform, message = message)
    return render_template('accountcreation.html', title='AccountCreation', userform = userform, createform = createform)
    
@app.route('/login', methods = ['GET','POST'])
def login_form():
    logged_in = "False"
    userform = LoginForm()
    failed = "False"
    if request.method == "POST":
        user = userform.userlogin.data
        password = userform.passlogin.data
        logged_in = find_user(user, password)
        if logged_in == "True":
            userinfo = pass_user_info(user)
            session['username'] = user
            return redirect('/assignments&u={0}'.format(user))
        else:
            failed = "True"
            return render_template('login.html', title='Test', userform = userform, user = user, password = password, logged_in = logged_in, failed = failed)
    return render_template('login.html', title='Login Form', userform = userform, logged_in = logged_in)
    
@app.route('/assignments&u=<user>', methods = ['GET', 'POST'])
def user_assignments(user):
    if 'username' in session:
        logged_in = "True"
        if session["username"] == user:
            assignmentform = AssignmentForm()
            statusform = StatusForm()
            userform = LoginForm()
            if request.method == "POST" and request.form['btn'] == 'Add':
                userinfo = pass_user_info(user)
                duedate = assignmentform.date.data
                tag = assignmentform.tag.data
                title = assignmentform.title.data
                if duedate != "" and title != "":
                    test = does_assignment_exist(session['username'], title, duedate)
                    if test != "Success":
                        add_assignment(session["username"], title, duedate, tag)
                assignments = get_assignments(user, "All")
                tags = grab_user_tags(user)
                print("success")
                return render_template('assignments.html', title='Assignments', user = userinfo, tags = tags, userform = userform, assignmentform = assignmentform, logged_in = logged_in, assignments = assignments, statusform = statusform)
            elif request.method == "POST" and request.form['btn'] == "Mark Checked Items Complete":
                userinfo = pass_user_info(user)
                assignments = get_assignments(user, "All")
                tags = grab_user_tags(user)
                for k, v in request.form.items():
                    if v != "Mark Checked Items Complete":
                        change_status(user, v)
                return render_template('assignments.html', logged_in = logged_in, title='Assignments', tags = tags, user = userinfo, userform = userform, assignmentform = assignmentform, assignments = assignments, statusform = statusform)
            else:
                userinfo = pass_user_info(user)
                assignments = get_assignments(user, "All")
                tags = grab_user_tags(user)
                return render_template('assignments.html', title='Assignments', logged_in = logged_in, tags = tags, user = userinfo, userform = userform, assignmentform = assignmentform, assignments = assignments, statusform = statusform)
        else:
            return redirect('/assignments&u={0}'.format(session["username"]))
    else:
        return redirect('/login')
        
@app.route('/editprofile&u=<user>', methods = ['GET', 'POST'])
def edit_profile(user):
    logged_in = "True"
    if request.method == "POST":
        createform = EditForm()
        fullname = createform.fullname.data
        twitter = createform.emailentry.data
        image = createform.image.data
        password = createform.password.data
        userform = LoginForm()
        confirmpassword = createform.confirmpassword.data
        if password != confirmpassword:
            message = "Password does not match"
            return render_template('EditProfile.html', title='Edit Profile', userform = userform, createform = createform, message = message)
        else:
            update_account_info(session['username'], fullname, password, twitter, image)
            return redirect('/assignments&u={0}'.format(session["username"]))
            
    if 'username' in session:
        if session["username"] == user:
            createform = EditForm()
            userform = LoginForm()
            user_info = pull_user_form_info(session['username'])
            createform.fullname.default = user_info['fullname']
            createform.emailentry.default = user_info['twitter']
            createform.password.default = user_info['password']
            createform.confirmpassword.default = user_info['password']
            createform.image.default = user_info['image']
            createform.process()
            return render_template('EditProfile.html', title='Edit Profile', logged_in = logged_in, createform = createform, userform = userform, user_info = user_info)
        else:
            return redirect('/editprofile&u={0}'.format(session['username']))
    else:
        return redirect('/login')
        
@app.route('/deleteassignment&u=<user>&assignment=<assignment>&date=<date>', methods = ['GET', 'POST'])
def deleteassignment(user, assignment, date):
    delete_assignment(user, assignment, date)
    return redirect('/assignments&u={0}'.format(session["username"]))
    
@app.route('/logout')
def logout():
    session.pop('username', None)
    return redirect(url_for('index'))

@app.route('/assignments&u=<user>&tag=<tag>', methods = ['GET', 'POST'])
def user_assignments_by_tags(user, tag):
    if 'username' in session:
        logged_in = "True"
        if session["username"] == user:
            assignmentform = AssignmentForm()
            statusform = StatusForm()
            userform = LoginForm()
            if request.method == "POST" and request.form['btn'] == 'Add':
                userinfo = pass_user_info(user)
                duedate = assignmentform.date.data
                tag = assignmentform.tag.data
                title = assignmentform.title.data
                if duedate != "" and title != "":
                    test = does_assignment_exist(session['username'], title, duedate)
                    if test != "Success":
                        add_assignment(session["username"], title, duedate, tag)
                assignments = get_assignments(user, tag)
                print("success")
                tags = grab_user_tags(user)
                return render_template('assignments.html', title='Assignments', user = userinfo, tags = tags, userform = userform, assignmentform = assignmentform, logged_in = logged_in, assignments = assignments, statusform = statusform)
            elif request.method == "POST" and request.form['btn'] == "Mark Checked Items Complete":
                userinfo = pass_user_info(user)
                assignments = get_assignments(user, tag)
                for k, v in request.form.items():
                    if v != "Mark Checked Items Complete":
                        change_status(user, v)
                tags = grab_user_tags(user)
                return render_template('assignments.html', logged_in = logged_in, title='Assignments', tags = tags, user = userinfo, userform = userform, assignmentform = assignmentform, assignments = assignments, statusform = statusform)
            else:
                userinfo = pass_user_info(user)
                assignments = get_assignments(user, tag)
                tags = grab_user_tags(user)
                return render_template('assignments.html', title='Assignments', logged_in = logged_in, tags = tags, user = userinfo, userform = userform, assignmentform = assignmentform, assignments = assignments, statusform = statusform)
        else:
            return redirect('/assignments&u={0}'.format(session["username"]))
    else:
        return redirect('/login')

@app.route('/edit_assignment&u=<user>&title=<title>&date=<date>&tag=<tag>', methods = ['GET', 'POST'])
def edit_existing_assignment(user, title, date, tag):
    if 'username' in session:
        logged_in = "True"
        if session["username"] == user:
            assignmentform = EditAssignment()
            userform = LoginForm()
            if request.method == "POST":
                newduedate = assignmentform.date.data
                newtag = assignmentform.tag.data
                newtitle = assignmentform.title.data
                if newduedate != "" and newtitle != "":
                    test = does_assignment_exist(session['username'], newtitle, newduedate)
                    if test != "Success":
                        update_assignment(session["username"], title, date, newtitle, newduedate, newtag)
                return redirect('/assignments')
            else:
                assignmentform.title.default = title
                assignmentform.date.default = date
                assignmentform.tag.default = tag
                assignmentform.process()
                return render_template('edit_assignment.html', title='Edit Assignment', userform =userform, logged_in = logged_in, assignmentform = assignmentform)
        else:
            return redirect('/edit_assignment&u={0}&title={1}&date={2}'.format(session["username"], title, date))
    else:
        return redirect('/login')