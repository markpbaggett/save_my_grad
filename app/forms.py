from flask.ext.wtf import Form
from wtforms import StringField, BooleanField, validators
from wtforms.validators import DataRequired

class LoginForm(Form):
    userlogin = StringField("userlogin")
    passlogin = StringField("passlogin")

class AssignmentForm(Form):
    title = StringField("title", [validators.DataRequired()])
    tag = StringField("tag")
    date = StringField("duedate", [validators.DataRequired()])

class UserForm(Form):
    username = StringField("userentry")
    fullname = StringField("fullname")
    emailentry = StringField("emailentry")
    phonenumber = StringField("phonenumber")
    password = StringField("password")
    confirmpassword = StringField("confirmpassword")
    image = StringField("image")
    
class EditForm(Form):
    username = StringField("userentry")
    fullname = StringField("fullname")
    emailentry = StringField("emailentry")
    phonenumber = StringField("phonenumber")
    password = StringField("password")
    confirmpassword = StringField("confirmpassword")
    image = StringField("image")
    
class StatusForm(Form):
    status1 = BooleanField()
    status2 = BooleanField()
    status3 = BooleanField()
    status4 = BooleanField()
    status5 = BooleanField()
    status6 = BooleanField()
    status7 = BooleanField()
    status8 = BooleanField()
    status9 = BooleanField()

class EditAssignment(Form):
    title = StringField("title", [validators.DataRequired()])
    tag = StringField("tag")
    date = StringField("duedate", [validators.DataRequired()])