from app import db

class User(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	username = db.Column(db.String(16), index=True, unique=True)
	fullname = db.Column(db.String(16), index=True, unique=False)
	password = db.Column(db.String(16), index=True, unique=False)
	image = db.Column(db.String(256), index=True, unique=False)
	twitter = db.Column(db.String(15), index=True, unique=True)
	work = db.relationship('Work', backref='author', lazy='dynamic')

	def __repr__(self):
		return '<User %r>' % (self.fullname)


class Work(db.Model):
	id = db.Column(db.Integer, primary_key = True)
	title = db.Column(db.String(140))
	duedate = db.Column(db.DateTime)
	tag = db.Column(db.String(140))
	status = db.Column(db.Boolean, default=False, nullable=True)
	user_id = db.Column(db.Integer, db.ForeignKey('user.id'))

	def __repr__(self):
		return '<Work %r>' % (self.title)
		
	