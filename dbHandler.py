from app import db, models
from datetime import datetime

def find_user(username, password): 
    users = models.User.query.all()
    success = False
    for user in users:
        if user.username == username and user.password == password:
            success = True
    success = str(success)
    return success;

def find_username(username): 
    find_users = models.User.query.all()
    success = False
    for user in find_users:
        if user.username == username:
            success = True
    success = str(success)
    return success;
    
        
def create_user(a,b,c,d, e): 
    new_user = models.User(username=a, fullname=b, password=c, twitter=d, image=e)
    db.session.add(new_user)
    db.session.commit()
    return a 


def pass_user_info(username):
    users = models.User.query.all()
    userinfo = "can't find"
    for user in users:
        if user.username == username:
            userinfo = {"username": user.username, "fullname": user.fullname, "image": user.image, "twitter": user.twitter}
    return userinfo
    
    
def pull_user_form_info(username):
    users = models.User.query.all()
    for user in users:
        if user.username == username:
            userinfo = {"fullname": user.username, "password": user.password, "image": user.image, "twitter": user.twitter}
    return userinfo
    
    
def update_account_info(username, fullname, password, twitter, image):
    user = models.User.query.filter_by(username=username).first()
    user.fullname = fullname
    user.password = password
    user.twitter = twitter
    user.image = image
    db.session.commit()
    
def get_assignments(username, tag):
    user = models.User.query.filter_by(username=username).first()
    user_id = user.id
    if tag == "All":
        assignments = models.Work.query.filter_by(user_id = user_id).order_by(models.Work.duedate)
        all_assignments = []
        for assignment in assignments:
            all_assignments.append(assignment)
        print(all_assignments)
        return all_assignments
    else:
        assignments = models.Work.query.filter_by(user_id = user_id).filter_by(tag = tag).order_by(models.Work.duedate)
        all_assignments = []
        for assignment in assignments:
            all_assignments.append(assignment)
        print(all_assignments)
        return all_assignments
        
def add_assignment(username, title, duedate, tag):
    datetime_object = datetime.strptime(duedate, '%m-%d-%Y')
    user = models.User.query.filter_by(username=username).first()
    user_id = user.id
    new_assignment = models.Work(title=title, duedate=datetime_object, tag=tag, user_id=user_id)
    db.session.add(new_assignment)
    db.session.commit()
    return "Success"
    
def change_status(username, title):
    user = models.User.query.filter_by(username=username).first()
    user_id = user.id
    find_assignment = models.Work.query.filter_by(user_id=user_id).filter_by(title=title).first()
    if find_assignment:
        find_assignment.status = True
        db.session.commit()
        print(find_assignment.status)
        return "Success"
    else:
        return "Nope"

def does_assignment_exist(username, title, duedate):
    datetime_object = datetime.strptime(duedate, '%m-%d-%Y')
    user = models.User.query.filter_by(username=username).first()
    user_id = user.id
    # find_assignment = models.Work.query.filter(models.Work.user_id==user_id, models.Work.duedate==datetime_object, models.Work.title==title)
    # (user_id=user_id, duedate=datetime_object, user_id=user_id)
    find_assignment = models.Work.query.filter_by(user_id=user_id).filter_by(duedate=datetime_object).filter_by(title=title).all()
    if find_assignment:
        return "Success"
    else:
        return "Nope"

def delete_assignment(username, title, date):
    datetime_object = datetime.strptime(date, '%Y-%m-%d')
    user = models.User.query.filter_by(username=username).first()
    user_id = user.id
    assignment = models.Work.query.filter_by(user_id=user_id).filter_by(duedate=datetime_object).filter_by(title=title).first()
    print(assignment)
    db.session.delete(assignment)
    db.session.commit()
    return "Success"
    
def grab_user_tags(username):
    user = models.User.query.filter_by(username=username).first()
    user_id = user.id
    tags = models.Work.query.filter_by(user_id = user_id)
    all_tags = []
    for assignment in tags:
        if assignment.tag not in all_tags:
            all_tags.append(assignment.tag)
            print(assignment.tag)
    return all_tags
    
def update_assignment(username, title, date, newtitle, newdate, newtag):
    datetime_object = datetime.strptime(date, '%Y-%m-%d')
    new_datetime_object = datetime.strptime(newdate, '%m-%d-%Y')
    user = models.User.query.filter_by(username=username).first()
    user_id = user.id
    assignment = models.Work.query.filter_by(user_id=user_id).filter_by(duedate=datetime_object).filter_by(title=title).first()
    assignment.title = newtitle
    assignment.duedate = new_datetime_object
    assignment.tag = newtag
    db.session.commit()

if __name__ == "__main__":
    # print(str(find_user('mellowmike', 'birdup')))
    # create_user('kimboslice', 'Kimbo Slice', 'slicedamice10', '@hedead')
    # add_assignment('mellowmike', 'Math Homework', 'Dec 28 2016', 'Math')
    # does_assignment_exist("pag", "Test", "12-29-2016")
    # change_status('mellowmike', 'Poker')
    # delete_assignment('mellowmike', 'test', '12-30-2016')
    # grab_user_tags('mellowmike')
    #get_assignments('mellowmike', 'Sports')
    update_assignment('mellowmike', 'Play Golf', '2017-01-12', 'Golf', '2017-02-26', 'Fun')